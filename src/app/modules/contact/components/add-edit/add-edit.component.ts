import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { contactService } from '../../../../shared/services/contactService';
import { CustomValidators } from '../../../../shared/validators/validator';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})

export class AddEditComponent implements OnInit {
  mobilesList: FormArray;
  contactForm: FormGroup;
  currId: number = null;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private formBuild: FormBuilder, private contactService: contactService) { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      name: new FormControl("", [Validators.required, CustomValidators.noWhitespaceValidator]),
      email: new FormControl("", [Validators.required, Validators.pattern(CustomValidators.email_pattern)]),
      mobilesArr: this.formBuild.array([this.createMobile()]),
    });
    
    this.activatedRoute.params.subscribe(
      params => {
        if (params.hasOwnProperty('id')) {
          this.currId = params.id;
          this.contactService.listById(params.id).subscribe(
            (response) => {
              this.contactForm.patchValue(response);
              this.contactForm.setControl('mobilesArr', this.formBuild.array((response.mobilesArr || []).map((x) => this.formBuild.group(x))));
            },
            (error: any) => { }
          );
        }
      });

    this.mobilesList = this.contactForm.get('mobilesArr') as FormArray;
  }

  getMobilesList(): FormArray {
    return <FormArray>this.contactForm.get('mobilesArr');
  }

  createMobile(): FormGroup {
    return this.formBuild.group({
      title: ['', [CustomValidators.noWhitespaceValidator]],
      number: ['', [Validators.pattern(CustomValidators.digit_pattern), Validators.maxLength(10), Validators.minLength(10), Validators.pattern('[0-9]+')]]
    });
  }

  addNewMobile(event) {
    event.preventDefault()
    this.mobilesList = this.contactForm.get('mobilesArr') as FormArray;
    this.mobilesList.push(this.createMobile());
  }

  removeMobile(index) {
    this.mobilesList.removeAt(index);
  }

  saveContact() {
    if (this.contactForm.valid) {
      let api;
      let contact = this.contactForm.value;

      if (this.currId) {
        contact.id = this.currId;
        api = this.contactService.update(contact);
      } else {
        api = this.contactService.add(contact);
      }

      api.subscribe(
        (response) => {
          this.router.navigate(['/contacts/list']);
        },
        (error: any) => { }
      );
    }
  }

  cancel() {
    this.router.navigate(['/contacts/list']);
  }
}
