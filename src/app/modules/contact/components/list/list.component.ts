import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { contactService } from '../../../../shared/services/contactService';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  displayedColumns: string[] = ['name', 'email', 'actions'];
  isRequestDone = false;
  sortedList;
  list: any = [];

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private router: Router, private contactService: contactService) { }

  ngOnInit() {
    this.isRequestDone = false;
    this.getContats();
  }

  public getContats(): void {
    this.contactService.list().subscribe(
      (response) => {
        this.list = response;
        this.setList(this.list);
        this.isRequestDone = true;
      },
      (error: any) => { }
    );
  }

  applyFilter(filterValue: string) {
    this.sortedList.filter = filterValue.trim().toLowerCase();
  }

  addNew() {
    this.router.navigate(['/contacts/add']);
  }

  editContact(contact) {
    this.router.navigate([`/contacts/edit/${contact.id}`]);
  }

  removeContact(contact, indx) {
    this.contactService.delete(contact).subscribe(
      (response) => {
        this.list.splice(indx, 1);
        this.setList(this.list);
      },
      (error: any) => { }
    );
  }

  setList(list) {
    this.sortedList = new MatTableDataSource(list);
    this.sortedList.sort = this.sort;
  }
}
