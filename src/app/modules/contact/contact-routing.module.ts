import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './components/list/list.component';
import { AddEditComponent } from './components/add-edit/add-edit.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: '/contacts/list',
        pathMatch: 'full'
    },
    {
        path: 'list',
        component: ListComponent
    },
    {
        path: 'add',
        component: AddEditComponent
    },
    {
        path: 'edit/:id',
        component: AddEditComponent
    },
    {
        path: '**',
        redirectTo: '/contacts/list',
    },
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: []
})

export class ContactRoutingModule { }