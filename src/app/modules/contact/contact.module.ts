import { NgModule } from '@angular/core';
import { SharedModule } from './../../shared/shared.module';
import { ListComponent } from './components/list/list.component';
import { AddEditComponent } from './components/add-edit/add-edit.component';
import { ContactRoutingModule } from './contact-routing.module';

@NgModule({
    imports: [SharedModule, ContactRoutingModule],
    exports: [],
    declarations: [ListComponent, AddEditComponent]
})

export class ContactModule { }