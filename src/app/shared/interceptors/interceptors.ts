import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpHeaders, HttpEvent } from '@angular/common/http';

@Injectable()

export class apiInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const authReq = req.clone({
            headers: new HttpHeaders({
                'token': 'MY_SECRET_ENCRYPTED_TOKEN'
            })
        });

        return next.handle(authReq);
    }
}